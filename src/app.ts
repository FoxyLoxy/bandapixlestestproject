import * as express from 'express';
import logger from 'morgan';
import * as bodyParser from "body-parser";
import helmet from "helmet";
import cors from "cors";

import routes from './routes';
import { handle404, handle500 } from "./controllers/app";

const app: express.Application = (express as any).default();

app.use(helmet());

app.use(cors());

app.use(
    logger('dev', {
        skip: () => app.get('env') === 'test',
    })
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true,
}));

app.use('/', routes);

app.use(handle404);
app.use(handle500);

export default app;
