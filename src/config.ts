interface Config {
    appPort: number;
    jwtSecret: string;
    dbName: string;
    latencyUrl: string;
}

const config = {
    appPort: process.env.PORT || 3000,
    jwtSecret: process.env.JWT_SECRET || "fallbackSecret",
    dbName: process.env.DB_NAME || "sqlite.db",
    latencyUrl: process.env.LATENCY_URL || "https://google.com",
} as Config;

export default config;
