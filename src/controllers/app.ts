import { Request, Response, NextFunction } from "express";
import { ValidationError } from "../errors/validationError";
import app from "../app";


export function handle500(err: Error, req: Request, res: Response, next: NextFunction) {
    switch (err.name) {
        case "ValidationError":
            res.status(400);
            if ((err as ValidationError).validationErrors) {
                res.json({
                    message: "invalid data passed",
                    validationErrors: (err as ValidationError).validationErrors,
                });
            } else {
                res.json({
                    message: "invalid data passed",
                });
            }
            break;
        case "AuthError":
            res.status(403);
            res.json({
                message: "unauthorized",
            });
            break;
        default:
            res.status(500);
            res.json(app.get('env') === 'test' ? err : {
                message: "internal server error",
            });
    }
}

export function handle404(req: Request, res: Response, next: NextFunction) {
    res.status(404);
    res.json({
        message: "Not found",
    });
}
