import { Request, Response } from "express";

import { signObject } from "../helpers/jwt";
import { addUser, authenticateUser} from "../models/crud/user";
import { addNewToken, revokeAllTokens, revokeToken} from "../models/crud/token";
import { extractTokenFromRequest } from "../helpers/jwt";
import { User } from "../models/user";
import { Token } from "../models/token";
import { ValidationError } from "../errors/validationError";
import { AuthError } from "../errors/authError";

export function signin(req: Request, res: Response) {
    const authenticatedUser = authenticateUser(req.body as User);
    if (!authenticatedUser) {
        throw new AuthError();
    } else {
        const token = signObject(authenticatedUser);
        const exp = Math.ceil(Date.now() / 1000) + 60 * 10;

        const createdToken = addNewToken({
            token: token,
            exp: exp,
        } as Token, authenticatedUser);

        if (createdToken) {
            res.json({
                user: {
                    id: authenticatedUser.id,
                    id_type: authenticatedUser.id_type
                },
                token: createdToken,
            });
        } else {
            throw new Error("cannot save token");
        }
    }
}

export function signup(req: Request, res: Response) {
    const createdUser = addUser({
        id: req.body.id as string,
        password: req.body.password as string,
    } as User);

    if (createdUser) {
        const token = signObject(createdUser);
        const exp = Math.ceil(Date.now() / 1000) + 60 * 10;

        const createdToken = addNewToken({
            token: token,
            exp: exp,
        } as Token, createdUser);

        if (createdToken) {
            res.json({
                user: {
                    id: createdUser.id,
                    id_type: createdUser.id_type
                },
                token: createdToken,
            });
        } else {
            throw new Error("cannot save token");
        }
    }
    throw new ValidationError([
        "this id is already taken",
    ]);
}

export function logout(req: Request, res: Response) {
    if (req.query.all && req.query.all === 'true') {
        const success = revokeAllTokens();
        if (success) {
            res.json({
                message: "successfully revoked all tokens",
            });
        }
    } else {
        const token = extractTokenFromRequest(req);
        if (token) {
            const success = revokeToken(token);
            if (success) {
                res.json({
                    message: "successfully revoked token",
                });
            }
        } else {
            throw new AuthError();
        }
    }
}
