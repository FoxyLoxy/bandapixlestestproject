import { Response } from "express";
import { RequestWithUser } from "../helpers/requestWithUser";
import * as request from "request";
import config from "../config";

export function info(req: RequestWithUser, res: Response) {
    const user = req.user;
    res.json({
        id: user.id,
        id_type: user.id_type,
    });
}

export function latency(req: RequestWithUser, res: Response) {
    request.get(config.latencyUrl,{
        time: true,
    },(error: any, response: request.Response, body: any) => {
        if (error === null && response.timings) {
            res.json({
                url: config.latencyUrl,
                latency: response.timings.connect,
            });
        } else {
            res.json({
                message: "Unable to communicate with server",
            });
        }
    });
}
