import { db } from "./sqlite";

const migrations = [
    "CREATE TABLE IF NOT EXISTS users (id VARCHAR(255) PRIMARY KEY NOT NULL,id_type VARCHAR(255)NOT NULL,password VARCHAR(255) NOT NULL);",
    "CREATE TABLE IF NOT EXISTS tokens (user_id VARCHAR(255) NOT NULL,token VARCHAR(255)NOT NULL,exp DATETIME NOT NULL, revoked INTEGER(1) DEFAULT 0, FOREIGN KEY (user_id) REFERENCES users(id));",
];

export function prepareDB(): void {
    migrations.forEach((sql: string) => {
        try {
            db.prepare(sql).run();
        } catch (e) {
            process.exit(1);
        }
    });
}
