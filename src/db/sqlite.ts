import sqlite3 from 'better-sqlite3';
import config from "../config";

export const db = new sqlite3(config.dbName);
