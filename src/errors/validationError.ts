export class ValidationError extends Error{
    public readonly validationErrors?: string[];

    constructor(validationErrors?: string[], message?: string) {
        super(message);
        this.name = "ValidationError";
        this.validationErrors = validationErrors;
    }
}
