import { Request } from "express";
import jwt from "jsonwebtoken";

import config from "../config";

export function extractTokenFromRequest(req: Request): string | undefined {
    const bearerHeader = req.header("Authorization");
    if (bearerHeader) {
        return bearerHeader.split(" ")[1];
    }
}

export function signObject(obj: any): string {
    return jwt.sign(obj, config.jwtSecret);
}
