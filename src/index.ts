import app from './app';
import { prepareDB } from "./db/migrate";
import config from "./config";

prepareDB();

app.listen(config.appPort).on("error", () => {
    process.exit(1);
});
