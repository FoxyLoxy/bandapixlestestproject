import { Response, Request, NextFunction } from 'express';
import exJwt from 'express-jwt';

import { extractTokenFromRequest } from "../helpers/jwt";
import { forgetToken, getToken, prolongToken, revokeToken } from "../models/crud/token";
import config from "../config";
import { AuthError } from "../errors/authError";

export function prolongJwtOnRequest(req: Request, res: Response, next: NextFunction) {
    const token = extractTokenFromRequest(req);
    if (token) {
        if (!prolongToken(token, 60 * 10)) {
            next(new Error("failed prolonging token"));
        }
    }
    next();
}

export const jwtMiddleware = exJwt({
    secret: config.jwtSecret,
    isRevoked: (req: Request, payload: any, done: (err: any, revoked?: boolean) => void) => {
        const tokenString = extractTokenFromRequest(req);
        if (tokenString) {
            const token = getToken(tokenString);

            if (token) {
                if (token.revoked) {
                    forgetToken(token.token);
                }

                if (!token.revoked && token.exp < Math.ceil(Date.now() / 1000)) {
                    if (!revokeToken(token.token)) {
                        done(new AuthError());
                    }
                    token.revoked = true;
                }

                done(null, token.revoked);
            } else {
                done(new AuthError());
            }
        } else {
            done(new AuthError());
        }
    },
});
