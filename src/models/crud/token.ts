import { db } from "../../db/sqlite";
import { User } from "../user";
import { Token } from "../token";

export function addNewToken(token: Token, user: User): Token | undefined {
    try {
        db.prepare("INSERT INTO tokens (user_id, token, exp) VALUES (?, ?, ?)").run(user.id, token.token, token.exp);
    } catch (e) {
        return undefined;
    }
    return token;
}

export function getToken(tokenString: string): Token | undefined {
    return  db.prepare("SELECT * FROM tokens WHERE token = ?").get(tokenString) as Token;
}

export function forgetToken(tokenString: string): void {
    db.prepare("DELETE FROM tokens WHERE token = ?").run(tokenString);
}

export function revokeToken(token: string): boolean {
    const result = db.prepare("UPDATE tokens SET revoked = 1 WHERE token = ?").run(token);
    return result.changes == 1;
}

export function revokeAllTokens(): boolean {
    try {
        db.prepare("UPDATE tokens SET revoked = 1").run();
    } catch (e) {
        return false;
    }
    return true;
}

export function prolongToken(token: string, seconds: number): boolean {
    const result = db.prepare("UPDATE tokens SET exp = exp + ? WHERE token = ?").run(seconds, token);
    return result.changes == 1;
}
