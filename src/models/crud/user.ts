import { validate as validateEmail } from "email-validator";
import { parsePhoneNumber } from "libphonenumber-js";
import bcrypt from "bcrypt";

import { db } from "../../db/sqlite";
import { idTypes, User } from "../user";
import { ValidationError } from "../../errors/validationError";

export function addUser(user: User): User | undefined {
    try {
        if (validateEmail(user.id)) {
            user.id_type = idTypes.EMAIL;
        } else if (parsePhoneNumber(user.id).isValid()) {
            const phoneObj = parsePhoneNumber(user.id);
            user.id = phoneObj.number as string;
            user.id_type = idTypes.PHONE;
        }

    } catch (e) {
        if (e.name === "ParseError") {
            throw new ValidationError([
                "id is not a phone number nor e-mail",
            ]);
        } else {
            throw e;
        }
    }

    user.password = bcrypt.hashSync(user.password, 10);

    try {
        const result = db.prepare("INSERT INTO users (id, id_type, password) VALUES (?, ?, ?)").run(user.id, user.id_type, user.password);

        if (result.changes == 1) {
            return user;
        }
    } catch (e) {
        return undefined;
    }
}

export function getUser(userId: string): User | undefined {
    const result = db.prepare(`SELECT * FROM users WHERE id = ?`).get(userId);
    if (result as User) {
        return result;
    }
}

export function authenticateUser(user: User): User | undefined {
    const dbUser = getUser(user.id);
    if (dbUser && bcrypt.compareSync(user.password, dbUser.password)) {
        return dbUser;
    }
}
