export interface Token {
    user_id: string;
    token: string;
    exp: number;
    revoked: boolean;
}
