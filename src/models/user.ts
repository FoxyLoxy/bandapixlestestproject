export enum idTypes {
    EMAIL = "email",
    PHONE = "phone",
}

export interface User {
    id: string;
    id_type: idTypes;
    password: string;
}
