import { Router, Request, Response } from 'express';

import { prolongJwtOnRequest, jwtMiddleware } from './middlewares/jwt';

import * as businessController  from './controllers/busieness';
import * as authController from "./controllers/auth";

const routes = Router();

routes.use(jwtMiddleware.unless({
    path: [
        '/signin',
        '/signup',
    ]
}));

routes.use(prolongJwtOnRequest);

routes.get('/info', businessController.info as (req: Request, res: Response) => void);
routes.get('/latency', businessController.latency as (req: Request, res: Response) => void);

routes.post('/signin', authController.signin);
routes.post('/signup', authController.signup);
routes.get('/logout', authController.logout);

export default routes;
